package com.muhammadibrahim.goldenscenttest.custom_control;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.muhammadibrahim.goldenscenttest.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muhammadibrahim on 6/18/18.
 */

public class CustomImageCarousal extends LinearLayout {

    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;

    private List<String> imagesList;
    private int index1 = 0;
    private int index2 = 1;
    private int index3 = 2;

    private Context context;

    public CustomImageCarousal(Context context) {
        super(context);
        if(imagesList == null){
            imagesList = new ArrayList<>();
        }
        this.context = context;
    }

    public CustomImageCarousal(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if(imagesList == null){
            imagesList = new ArrayList<>();
        }
        this.context = context;

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.custom_image_carousal, this);

        loadViews();
        init();
    }

    private void loadViews(){
        imageView1 = findViewById(R.id.image1);
        imageView2 = findViewById(R.id.image2);
        imageView3 = findViewById(R.id.image3);
    }

    private void init(){
        loadImagesBitmap();
    }


    public void setImagesList(List<String> imagesList){
        this.imagesList.clear();
        this.imagesList.addAll(imagesList);
        loadImagesBitmap();
        invalidate();
        requestLayout();
    }


    /**
     * anti-clock wise direction.
     * when right button is clicked,
     * move images from right to left.
     */
    public void shiftLift(){

        int count = imagesList.size(); //7

        index3++;
        if(index3 > count-1){
            index3 = 0;
        }

        index2++;
        if(index2 > count-1){
            index2 = 0;
        }

        index1++;
        if(index1 > count-1){
            index1 = 0;
        }

        loadImagesBitmap();

        invalidate();
        requestLayout();
    }

    /**
     * clock wise direction.
     * when left button is clicked,
     * move images from left to right.
     */
    public void shiftRight(){
        int count = imagesList.size();

        index3--;
        if(index3 < 0){
            index3 = count-1;
        }

        index2--;
        if(index2 < 0){
            index2 = count-1;
        }

        index1--;
        if(index1 < 0){
            index1 = count-1;
        }

        loadImagesBitmap();

        invalidate();
        requestLayout();

    }

    private void loadImagesBitmap(){
        try {
            Picasso.with(context).load(imagesList.get(index1)).placeholder(R.drawable.image_preview).into(imageView1);
            Picasso.with(context).load(imagesList.get(index2)).placeholder(R.drawable.image_preview).into(imageView2);
            Picasso.with(context).load(imagesList.get(index3)).placeholder(R.drawable.image_preview).into(imageView3);
        }
        catch (Exception e){

        }
    }
}
