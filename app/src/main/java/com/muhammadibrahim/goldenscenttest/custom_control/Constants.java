package com.muhammadibrahim.goldenscenttest.custom_control;

/**
 * Created by muhammadibrahim on 6/18/18.
 */

public class Constants {

    public static final String VIDEO_URI = "http://184.72.239.149/vod/smil:BigBuckBunny.smil/playlist.m3u8";
    //public static final String VIDEO_URI = "http://playertest.longtailvideo.com/adaptive/wowzaid3/playlist.m3u8";
    //public static final String VIDEO_URI = "http://clips.vorwaerts-gmbh.de/VfE_html5.mp4";
    public static final String IMAGES_BASE_URI = "https://picsum.photos/400?image=";

    public static final String VIDEO_SEEKBAR_POSITION = "videoSeekbarPosition";
}
