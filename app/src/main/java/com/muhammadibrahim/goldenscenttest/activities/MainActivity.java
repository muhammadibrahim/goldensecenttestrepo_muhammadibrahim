package com.muhammadibrahim.goldenscenttest.activities;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.muhammadibrahim.goldenscenttest.R;
import com.muhammadibrahim.goldenscenttest.custom_control.Constants;
import com.muhammadibrahim.goldenscenttest.custom_control.CustomImageCarousal;
import com.muhammadibrahim.goldenscenttest.mvc_architecture.presenter.IPresenter;
import com.muhammadibrahim.goldenscenttest.mvc_architecture.presenter.PresenterImpl;
import com.muhammadibrahim.goldenscenttest.mvc_architecture.view.IMainView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements IMainView {

    @BindView(R.id.videoView)
    VideoView videoView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.carousalImages)
    CustomImageCarousal imageCarousal;
    @BindView(R.id.nextBtn)
    Button nextBtn;
    @BindView(R.id.prevBtn)
    Button prevBtn;

    IPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupVideo(savedInstanceState);

        presenter = new PresenterImpl(this);
        presenter.initializeImagesUri();
    }


    private void setupVideo(Bundle savedInstanceState) {

        MediaController controller = new MediaController(this);
        controller.setAnchorView(videoView);
        videoView.setMediaController(controller);

        videoView.setVideoPath(Constants.VIDEO_URI);

        if(savedInstanceState != null){
            int currentPos = savedInstanceState.getInt(Constants.VIDEO_SEEKBAR_POSITION);

            if(currentPos>1000){
                currentPos = currentPos-1000;
            }
            else{
                currentPos = 0;
            }
            videoView.seekTo(currentPos);
        }

        videoView.setOnPreparedListener(preparedListener);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            videoView.setOnInfoListener(infoListener);
        }

    }


    //region overriden methods from IMainView interface

    @Override
    public void showProgressBar(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setImagesUri(List<String> imagesUri) {
        imageCarousal.setImagesList(imagesUri);
    }

    //endregion


    //region listener
    private MediaPlayer.OnPreparedListener preparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            progressBar.setVisibility(View.GONE);
            if(mp.isPlaying()){
                mp.stop();
                mp.release();
                mp = new MediaPlayer();
            }
            mp.setVolume(0,0);
            mp.setLooping(true);
            mp.start();
        }
    };

    private MediaPlayer.OnInfoListener infoListener = new MediaPlayer.OnInfoListener() {
        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            switch (what){
                case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                    progressBar.setVisibility(View.GONE);
                    return true;
                case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                    progressBar.setVisibility(View.VISIBLE);
                    return true;
                case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                    progressBar.setVisibility(View.GONE);
                    return true;
            }
            return false;
        }
    };


    @OnClick({R.id.prevBtn, R.id.nextBtn})
    public void onNavigationBtnClick(View view){
        switch (view.getId()){
            case R.id.prevBtn:
                imageCarousal.shiftRight();
                break;
            case R.id.nextBtn:
                imageCarousal.shiftLift();
                break;
        }
    }

    //endregion


    //region activity overriden methods
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.VIDEO_SEEKBAR_POSITION, videoView.getCurrentPosition());
        super.onSaveInstanceState(outState);
    }
    //endregion
}