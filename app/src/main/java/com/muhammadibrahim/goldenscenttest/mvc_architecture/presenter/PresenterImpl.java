package com.muhammadibrahim.goldenscenttest.mvc_architecture.presenter;

import android.os.AsyncTask;

import com.muhammadibrahim.goldenscenttest.custom_control.Constants;
import com.muhammadibrahim.goldenscenttest.mvc_architecture.view.IMainView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by muhammadibrahim on 6/18/18.
 */

public class PresenterImpl implements IPresenter {

    IMainView mainView;

    public PresenterImpl(IMainView mainView){
        this.mainView = mainView;
    }

    @Override
    public void initializeImagesUri() {
        mainView.showProgressBar(true);
        new InitializeUriAsync().execute();
    }

    public class InitializeUriAsync extends AsyncTask<Void, Void, List<String>>{

        @Override
        protected List<String> doInBackground(Void... voids) {
            List<String> list = new ArrayList<>();
            Random random = new Random();
            for(int i=0; i<7; i++){
                int imageId = random.nextInt(999)+10;
                list.add(Constants.IMAGES_BASE_URI+imageId);
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            mainView.setImagesUri(strings);
            mainView.showProgressBar(false);
        }
    }
}
