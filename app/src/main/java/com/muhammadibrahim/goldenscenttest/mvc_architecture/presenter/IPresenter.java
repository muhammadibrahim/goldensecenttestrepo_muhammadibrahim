package com.muhammadibrahim.goldenscenttest.mvc_architecture.presenter;

/**
 * Created by muhammadibrahim on 6/18/18.
 */

public interface IPresenter {
    /**
     * Generates seven images uri's asynchrously.
     * generate a random number and append that
     * number with base_image_uri
     * from {@link com.muhammadibrahim.goldenscenttest.custom_control.Constants}
     */
    void initializeImagesUri();
}
