package com.muhammadibrahim.goldenscenttest.mvc_architecture.view;

import java.util.List;

/**
 * Created by muhammadibrahim on 6/18/18.
 */

public interface IMainView {
    void showProgressBar(boolean show);
    void setImagesUri(List<String> imagesUri);
}
